from django.test import TestCase
from productos.models import Categorias
# Create your tests here.

NOMBRE = 'Prueba'

# Nodels
class CrearCategoria(TestCase):
    def setUp(self):
        self.cat = Categorias.objects.create(nombre = NOMBRE)
    
    def test_categorias_model(self):
        categoria = Categorias.objects.get(nombre = NOMBRE)
        self.assertEqual(categoria.nombre, NOMBRE)